<?php

namespace Vespula\Cache\Exception\File;

use Psr\SimpleCache\InvalidArgumentException as SimpleCacheInvalidArgumentException;

class InvalidPathException extends \InvalidArgumentException implements SimpleCacheInvalidArgumentException
{
}