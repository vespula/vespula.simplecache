<?php

namespace Vespula\Cache\Exception\File;

use Psr\SimpleCache\CacheException;

class FileLockException extends \Exception implements CacheException
{
}