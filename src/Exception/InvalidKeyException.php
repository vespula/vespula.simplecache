<?php

namespace Vespula\Cache\Exception;

use Psr\SimpleCache\InvalidArgumentException as SimpleCacheInvalidArgumentException;

class InvalidKeyException extends \InvalidArgumentException implements SimpleCacheInvalidArgumentException
{
}