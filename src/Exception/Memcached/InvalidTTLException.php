<?php

namespace Vespula\Cache\Exception\Memcached;

use Psr\SimpleCache\InvalidArgumentException as SimpleCacheInvalidArgumentException;

class InvalidTTLException extends \InvalidArgumentException implements SimpleCacheInvalidArgumentException
{
}