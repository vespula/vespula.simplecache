<?php 
declare(strict_types=1);

namespace Vespula\Cache\Adapter;

use Vespula\Cache\Exception\Sql\PdoStatementException;
use DateInterval;

/**
 * A simple sql caching adapter
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 */
class Sql extends AbstractAdapter 
{
    /**
     * Instance of PHP PDO
     * 
     * @var \PDO
     */
    protected $pdo;

    /**
     * The cache table in the DB
     * 
     * @var string
     */
    protected $table;



    /**
     * Constructor
     * 
     * @param \PDO $pdo
     * @param int|DateInterval $default_ttl Default expiry in seconds, or DateInterval object. 0 for no expiry
     * @param string $table The table in the DB to store items
     */
    public function __construct(\PDO $pdo, $default_ttl, string $table)
    {
        $this->pdo = $pdo;
        $this->table = $table;
        $this->default_ttl = $this->formatTTL($default_ttl);

    }

    /**
     * Get a value from the cache by key name
     * 
     * @param string $key
     * @param mixed $default The value to return if the key was not previously set, or it expired.
     * @return mixed The value that was stored, or the default on a miss.
     */
    public function get($key, $default = null)
    {
        $this->checkKey($key);
        
        $query = "SELECT * from {$this->table} where id = ?";
        $stmt = $this->pdo->prepare($query);
        $success = $stmt->execute([$key]);

        if ($success === false) {
            return $default;
        }

        $row = $stmt->fetchObject();

        if (! $row) {
            return $default;
        }
        
        $value = unserialize($row->content);

        $modtime = strtotime($row->timestamp);
        $expired = $this->isExpired($modtime, (int) $row->ttl);

        if ($expired) {
            $this->delete($key);
            return $default;
        }

        return $value;

    }

    /**
     * Cache a value identified by key with optional expiry
     * 
     * @param string $key
     * @param mixed $value
     * @param integer $ttl Number of seconds or 0 for no expiry
     * @return bool True on success, false on failure
     */
    public function set($key, $value, $ttl = null): bool
    {
        $this->checkKey($key);

        $value = serialize($value);
        $now = date('Y-m-d H:i:s');

        if ($ttl) {
            $ttl = $this->formatTTL($ttl);
        }
        $item_ttl = $ttl ?? $this->default_ttl;

        $update_query  = "UPDATE {$this->table} SET content = ?, ttl = ?, `timestamp` = ? where id = ?";
        $stmt = $this->pdo->prepare($update_query);
        $stmt->execute([
            $value,
            $item_ttl,
            $now,
            $key]
        );
        $rows = $stmt->rowCount();

        if ($rows === 1) {
            return true;
        }
        
        $insert_query  = "INSERT INTO {$this->table} (id, content, `timestamp`, ttl) 
                            values (?, ?, ?, ?)";
        $stmt = $this->pdo->prepare($insert_query);
        $stmt->execute([
            $key,
            $value,
            $now,
            $item_ttl]
        );
        

        $rows = $stmt->rowCount();
        if ($rows === 1) {
            return true;
        }

        return false;
        
    }

    /**
     * Delete a single cache entry by key
     * 
     * @param string $key
     * @return bool
     */
    public function delete($key)
    {
        $this->checkKey($key);

        
        $query = "DELETE from {$this->table} where id = ?";
        $stmt = $this->pdo->prepare($query);
        $success = $stmt->execute([$key]);

        // return $success; // Or show some info?

        if ($success === false) {
            $error_info = $stmt->errorInfo();
            if ($error_info[0] != '00000') {
                throw new PdoStatementException($error_info[2] ?? 'Unknown PDO Statement error');
            }
            return false;
        }

        return true;
    }

    /**
     * Clear all cached values
     * 
     * @return bool True on success, false on failure
     */
    public function clear()
    {
        $query = "DELETE from {$this->table}";

        $success = $this->pdo->exec($query);

        return $success !== false;
    }

    /**
     * Get multiple values via an array (iterable) of keys. Return default value on miss
     * 
     * @param iterable $keys
     * @param mixed $default A default value on cache miss
     * @return array of key value pairs
     */
    public function getMultiple($keys, $default = null)
    {
        $store = [];
        foreach ($keys as $key) {
            $store[$key] = $this->get($key, $default);
        }
        
        return $store;
    }

    /**
     * Set multipe cache items at once
     * 
     * @param iterable $values Key Value pairs
     * @param integer $ttl The expiry or the items if overriding the default. 0 for no expiry.
     * @return bool True on success, false on failure
     */
    public function setMultiple($values, $ttl = null)
    {
        $didSet = [];
        $count = 0; // Can't use count() on traversable
        $this->pdo->beginTransaction();
        foreach ($values as $key=>$value) {
            $count++;
            if ($this->set($key, $value, $ttl)) {
                $didSet[] = $key;
            }
        }

        if (count($didSet) == $count) {
            $this->pdo->commit();
            return true;
        }
        $this->pdo->rollBack();
        
        return false;
    }

    /**
     * Delete multiple cache entries
     * 
     * @param iterable $keys
     * @return bool
     */
    public function deleteMultiple($keys)
    {
        $success = true;
        foreach ($keys as $key) {
            $deleted = $this->delete($key);
            if (! $deleted) {
                $success = false;
            }
        }

        return $success;
    }

    /**
     * Determine if the storage has a particular value by key
     * 
     * @param string $key
     * @return bool
     */
    public function has($key)
    {
        // Not to be used in other methods such as get and set as it could be unreliable if timing is off.
        $this->checkKey($key);

        $query = "SELECT id, `timestamp`, ttl from {$this->table} where id = ?";
        $stmt = $this->pdo->prepare($query);
        $success = $stmt->execute([$key]);

        if (! $success) {
            return false;
        }

        $row = $stmt->fetchObject();
        if  (! $row) {
            return false;
        }
        $modtime = strtotime($row->timestamp);
        $ttl = (int) $row->ttl;

        if ($this->isExpired($modtime, $ttl)) {
            $this->delete($key);
            return false;
        }
        return true;
        
    }

    /**
     * Update the expiry for a keyed value
     * 
     * @param string $key
     * @param integer $ttl
     * @return bool
     */
    public function touch(string $key, int $ttl)
    {
        $this->checkKey($key);
        $value = $this->get($key);

        if (! $value) {
            return false;
        }

        return $this->set($key, $value, $ttl);
    }

}