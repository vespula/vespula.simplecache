<?php 
declare(strict_types=1);

namespace Vespula\Cache\Adapter;

use \Memcached as PHPMemcached;
use Vespula\Cache\Exception\Memcached\InvalidTTLException;
use DateInterval;

/**
 * A simple memcached caching adapter
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 */
class Memcached extends AbstractAdapter 
{
    /**
     * An instance of PHP Memcached
     * 
     * @var \Memcached
     */
    protected $phpmemcached;

    
    /**
     * Constructor
     * 
     * @param \Memcached $phpmemcached The PHP Memcached instance
     * @param integer|DateInterval $default_ttl Default expiry in seconds. 0 for no expire, or a DateInterval
     */
    public function __construct(PHPMemcached $phpmemcached, $default_ttl)
    {
        $this->phpmemcached = $phpmemcached;

        $this->default_ttl = $this->formatTTL($default_ttl);

        // Memcached supports timestamp or number of seconds* for ttl. Let's force number of seconds.
        // Memcached will assume timestamp if the number is > seconds in 30 days.
        // * number of seconds refering to an age in seconds. eg. 3600 = expire in 1 hour
        $this->checkTTL($this->default_ttl);

    }

    /**
     * Get the PHP Memcached instance
     * 
     * @return \Memcached
     */
    public function getPHPmemcached(): \Memcached
    {
        return $this->phpmemcached;
    }

    /**
     * Cache a value identified by key with optional expiry
     * 
     * @param string $key
     * @param mixed $value
     * @param int|DateInterval $ttl Number of seconds, DateInterval object or 0 for no expiry, or null for default
     * @return bool True on success, false on failure
     */
    public function set($key, $value, $ttl = null)
    {
        $this->checkKey($key);


        if ($ttl) {
            $ttl = $this->formatTTL($ttl);
            $this->checkTTL($ttl);
        }
        $item_ttl = $ttl ?? $this->default_ttl;

        return $this->phpmemcached->set($key, $value, $item_ttl);

    }

    /**
     * Get a value from the cache by key name
     * 
     * @param string $key
     * @param mixed $default The value to return if the key was not previously set, or it expired.
     * @return mixed The value that was stored, or the default on a miss.
     */
    public function get($key, $default = null)
    {
        $this->checkKey($key);
        $value = $this->phpmemcached->get($key);

        if ($value === false) {
            return $default;
        }
        return $value;
    }

    /**
     * Determine if the storage has a particular value by key
     * 
     * @param string $key
     * @return bool
     */
    public function has($key): bool
    {
        $this->checkKey($key);
        $value = $this->phpmemcached->get($key);

        return $value !== false;
    }

    /**
     * Delete a single cache entry by key
     * 
     * @param string $key
     * @return bool
     */
    public function delete($key): bool
    {
        $this->checkKey($key);
        return $this->phpmemcached->delete($key);
    }

    /**
     * Clear all cached values
     * 
     * @return bool True on success, false on failure
     */
    public function clear(): bool
    {
        return $this->phpmemcached->flush();
    }

    /**
     * Set multipe cache items at once
     * 
     * @param iterable $values Key Value pairs
     * @param integer $ttl The expiry or the items if overriding the default. 0 for no expiry.
     * @return bool True on success, false on failure
     */
    public function setMultiple($values, $ttl = null): bool
    {
        $this->checkTTL($ttl);

        // Need to ensure array type for the values
        $values = (array) $values;
        if (! $ttl) {
            $ttl = $this->default_ttl;
        }
        return $this->phpmemcached->setMulti($values, $ttl);
    }

    /**
     * Get multiple values via an array (iterable) of keys. Return default value on miss
     * 
     * @param iterable $keys
     * @param mixed $default A default value on cache miss
     * @return iterable of key value pairs
     */
    public function getMultiple($keys, $default = null): iterable
    {
        // Need to ensure array type for the keys
        $keys = (array) $keys;
        $this->checkKeys($keys);

        $cached = $this->phpmemcached->getMulti($keys, PHPMemcached::GET_PRESERVE_ORDER);
        if ($cached === false) {
            throw new \MemcachedException($this->phpmemcached->getResultMessage());
        }

        
        foreach ($cached as $key=>$value) {
            if ($value === null) {
                $cached[$key] = $default;
            }
        }

        return $cached;
    }
    
    /**
     * Delete multiple cache entries
     * 
     * @param iterable $keys
     * @return bool
     */
    public function deleteMultiple($keys): bool
    {
        $keys = (array) $keys;
        $this->checkKeys($keys);
        $results = $this->phpmemcached->deleteMulti($keys);
        
        foreach ($results as $result) {
            if ($result === true) {
                continue;
            }
            if ($result === false) {

                return false; // I don't think is occurs
            }
            if (is_int($result)) {
                if ($result !== PHPMemcached::RES_NOTFOUND && $result !== PHPMemcached::RES_NOTSTORED) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Update the expiry for a keyed value
     * 
     * @param string $key
     * @param integer $ttl
     * @return bool
     */
    public function touch(string $key, int $ttl): bool
    {
        $this->checkKey($key);
        return $this->phpmemcached->touch($key, $ttl);
    }

    /**
     * Disconnect from memcached server
     * 
     * @return bool
     */
    public function quit(): bool
    {
        return $this->phpmemcached->quit();
    }

    

    /**
     * Check that the TTL value is valid (less than seconds in 30 days)
     * 
     * @param int $ttl
     * @return void
     * @throws InvalidTTLException
     */
    protected function checkTTL(int $ttl = null): void 
    {
        if ($ttl === null) {
            return;
        }
        if ($ttl > (60*60*24*30)) {
            throw new InvalidTTLException('Number of seconds for default TTL is greather than max');
        }
    }
}