<?php 
declare(strict_types=1);

namespace Vespula\Cache\Adapter;

use Predis\Client as PredisClient;
use DateInterval;

/**
 * A simple Redis caching adapter using Predis as the Redis Client for PHP
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 */
class Redis extends AbstractAdapter 
{
    /**
     * An instance of Predis Client
     * 
     * @var PredisClient
     */
    protected $predisClient;

    
    /**
     * Constructor
     * 
     * @param PredicClient $predisClient The Predis Client instance
     * @param integer|DateInterval $default_ttl Default expiry in seconds. 0 for no expire, or a DateInterval
     */
    public function __construct(PredisClient $predisClient, $default_ttl)
    {
        $this->predisClient = $predisClient;

        $this->default_ttl = $this->formatTTL($default_ttl);

    }

    /**
     * Get the Predis client instance
     * 
     * @return PredisClient
     */
    public function getPredisClient(): PredisClient
    {
        return $this->predisClient;
    }

    /**
     * Cache a value identified by key with optional expiry
     * 
     * @param string $key
     * @param mixed $value
     * @param int|DateInterval $ttl Number of seconds, DateInterval object or 0 for no expiry, or null for default
     * @return bool True on success, false on failure
     */
    public function set($key, $value, $ttl = null)
    {
        $this->checkKey($key);


        if ($ttl) {
            $ttl = $this->formatTTL($ttl);
        }
        $item_ttl = $ttl ?? $this->default_ttl;

        // Redis generally stores strings only so let's serialize to maintain types
        $serialized = serialize($value);

        if ($item_ttl === 0) {
            $status = $this->predisClient->set($key, $serialized);
            return $status->getPayload() == 'OK';
        }

        // EX indicates the ttl is in seconds
        $status = $this->predisClient->set($key, $serialized, 'EX', $item_ttl);
        return $status->getPayload() == 'OK';

    }

    /**
     * Get a value from the cache by key name
     * 
     * @param string $key
     * @param mixed $default The value to return if the key was not previously set, or it expired.
     * @return mixed The value that was stored, or the default on a miss.
     */
    public function get($key, $default = null)
    {
        $this->checkKey($key);
        $value = $this->predisClient->get($key);

        if ($value === NULL) {
            return $default;
        }
        // Was serialized on set, so unserialize
        return unserialize($value);
    }

    /**
     * Determine if the storage has a particular value by key
     * 
     * @param string $key
     * @return bool
     */
    public function has($key): bool
    {
        $this->checkKey($key);
        $value = $this->predisClient->exists($key);

        return $value === 1 ? true : false;
    }

    /**
     * Delete a single cache entry by key
     * 
     * @param string $key
     * @return bool
     */
    public function delete($key): bool
    {
        $this->checkKey($key);
        // del() returns the number of keys that were removed. Should be 1 in this case
        $value = $this->predisClient->del($key);

        return $value === 1 ? true : false;
    }

    /**
     * Clear all cached values
     * 
     * @return bool True on success, false on failure
     */
    public function clear(): bool
    {
        $status = $this->predisClient->flushdb();
        return $status->getPayload() === 'OK';
    }

    /**
     * Set multipe cache items at once
     * 
     * @param iterable $values Key Value pairs
     * @param integer $ttl The expiry or the items if overriding the default. 0 for no expiry.
     * @return bool True on success, false on failure
     */
    public function setMultiple($values, $ttl = null): bool
    {

        // Need to ensure array type for the values
        $values = (array) $values;
        if (! $ttl) {
            $ttl = $this->default_ttl;
        }

        // serialize each value
        foreach ($values as $key=>$value) {
            $values[$key] = serialize($value);
        }

        $status = $this->predisClient->mset($values);
        // Need to set the expiry on all those keys now. MSET does not support that.
        foreach (array_keys($values) as $key) {
            $this->predisClient->expire($key, $ttl);
        }

        // MSET does not fail, so it always will be true
        return true;
    }

    /**
     * Get multiple values via an array (iterable) of keys. Return default value on miss
     * 
     * @param iterable $keys
     * @param mixed $default A default value on cache miss
     * @return iterable of key value pairs
     */
    public function getMultiple($keys, $default = null): iterable
    {
        // Need to ensure array type for the keys
        $keys = (array) $keys;
        $this->checkKeys($keys);

        $values = $this->predisClient->mget($keys);
        
        foreach ($values as $index=>$value) {
            if ($value === null) {
                $values[$index] = $default;
            } else {
                $values[$index] = unserialize($value);
            }
        }

        return array_combine($keys, $values);
    }
    
    /**
     * Delete multiple cache entries
     * 
     * @param iterable $keys
     * @return bool if the number of keys is greater than 0
     */
    public function deleteMultiple($keys): bool
    {
        $keys = (array) $keys;
        $this->checkKeys($keys);
        $count = $this->predisClient->del($keys);
        
        // as long as at least one key existed and was deleted
        return $count > 0;
    }

    /**
     * Update the expiry for a keyed value
     * 
     * @param string $key
     * @param integer $ttl
     * @return bool
     */
    public function touch(string $key, int $ttl): bool
    {
        $this->checkKey($key);
        $this->predisClient->touch($key);
        return $this->predisClient->expire($key, $ttl) === 1;
    }

    /**
     * Disconnect from memcached server
     * 
     * @return bool
     */
    public function quit(): bool
    {
        $this->predisClient->quit();
        return true; // always ok
    }
}