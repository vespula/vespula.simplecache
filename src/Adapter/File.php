<?php

declare(strict_types=1);

namespace Vespula\Cache\Adapter;

use SplFileObject;
use Vespula\Cache\Exception\File\InvalidPathException;
use Vespula\Cache\Exception\File\FileLockException;
use DateInterval;

/**
 * A simple file caching adapter
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 */
class File extends AbstractAdapter
{
    /**
     * The path for storing cache files. Must be read/write by php / web server process
     * 
     * @var string
     */
    protected $path;


    /**
     * Constructor
     * 
     * @param string $path Path for cache files
     * @param integer|DateInterval $default_ttl The default expiry in seconds. 0 for no expiry.
     */
    public function __construct(string $path, $default_ttl)
    {
        $path = realpath($path);


        if (! $path || ! file_exists($path)) {
            throw new InvalidPathException("Path does not exist");
        }
        if (! is_writable($path)) {
            throw new InvalidPathException("Directory `$path` is not writable");
        }
        if (! is_readable($path)) {
            throw new InvalidPathException("Directory `$path` is not readable");
        }

        $this->path = $path;

        $this->default_ttl = $this->formatTTL($default_ttl);
    }

    /**
     * Get a value from the cache by key name
     * 
     * @param string $key
     * @param mixed $default The value to return if the key was not previously set, or it expired.
     * @return mixed The value that was stored, or the default on a miss.
     */
    public function get($key, $default = null)
    {
        $this->checkKey($key);
        
        $filename = $this->path . '/vespula-cache-' . $key;

        if (! file_exists($filename) || ! is_readable($filename)) {
            return $default;
        }

        $file = new \SplFileObject($filename);
        $file->rewind();
        $ttl = (int) $file->current();

        $modtime = filemtime($filename);
        $expired = $this->isExpired($modtime, $ttl);
        if ($expired) {
            $this->delete($key);
            return $default;
        }

        $file->next();
        $serialized = '';
        while (! $file->eof()) {
            $serialized .= $file->current();
            $file->next();
        }
        unset($file);        
        $value = unserialize($serialized);

        return $value;
    }

    /**
     * Cache a value identified by key with optional expiry
     * 
     * @param string $key
     * @param mixed $value
     * @param integer $ttl Number of seconds or 0 for no expiry
     * @return bool True on success, false on failure
     */
    public function set($key, $value, $ttl = null): bool
    {
        $this->checkKey($key);

        $filename = $this->path . '/vespula-cache-' . $key;
        
        if ($ttl) {
            $ttl = $this->formatTTL($ttl);
        }
        $item_ttl = $ttl ?? $this->default_ttl;

        

        $data = [
            str_pad((string) $item_ttl, 20, '-'),
            serialize($value)
        ];
        $contents = implode(PHP_EOL, $data);
        $file = new SplFileObject($filename, 'w');
        if (! $file->flock(LOCK_EX)) {
            throw new FileLockException('Could not get file lock');
        }

        $file->ftruncate(0);
        $bytes = $file->fwrite($contents);
        $file->fflush();
        $file->flock(LOCK_UN);
        if ($bytes === false) {
            throw new \Exception('Could not write to the file');
        }
        unset($file);
        return true;
    }

    /**
     * Delete a single cache entry by key
     * 
     * @param string $key
     * @return bool
     */
    public function delete($key): bool
    {
        $this->checkKey($key);


        $filename = $this->path . '/vespula-cache-' . $key;
        if (file_exists($filename)) {
            return unlink($filename);
        }
        
        // Nothing to delete so it should be considered gone.
        // Maybe this thinking is not correct, but I don't see this as an error
        return true;
    }

    /**
     * Clear all cached values
     * 
     * @return bool True on success, false on failure
     */
    public function clear(): bool
    {
        $success = false;
        $files = glob($this->path . '/vespula-cache-*');
        foreach ($files as $file) {
            $success = unlink($file);
        }

        return $success;
    }

    /**
     * Get multiple values via an array (iterable) of keys. Return default value on miss
     * 
     * @param iterable $keys
     * @param mixed $default A default value on cache miss
     * @return array of key value pairs
     */
    public function getMultiple($keys, $default = null): array
    {
        $store = [];
        foreach ($keys as $key) {
            $store[$key] = $this->get($key, $default);
        }
        
        return $store;
    }

    /**
     * Set multipe cache items at once
     * 
     * @param iterable $values Key Value pairs
     * @param integer $ttl The expiry or the items if overriding the default. 0 for no expiry.
     * @return bool True on success, false on failure
     */
    public function setMultiple($values, $ttl = null): bool
    {
        $didSet = [];
        $count = 0; // Can't use count() on traversable
        foreach ($values as $key=>$value) {
            $count++;
            if ($this->set($key, $value, $ttl)) {
                $didSet[] = $key;
            }
        }

        if (count($didSet) == $count) {
            return true;
        }
        foreach ($didSet as $key) {
            $this->delete($key);
        }
        return false;
    }

    /**
     * Delete multiple cache entries
     * 
     * @param iterable $keys
     * @return bool
     */
    public function deleteMultiple($keys): bool
    {
        $success = true;
        foreach ($keys as $key) {
            $deleted = $this->delete($key);
            if (! $deleted) {
                $success = false;
            }
        }

        return $success;
    }

    /**
     * Determine if the storage has a particular value by key
     * 
     * @param string $key
     * @return bool
     */
    public function has($key): bool
    {
        // Not to be used in other methods such as get and set as it could be unreliable if timing is off.
        $this->checkKey($key);

        $filename = $this->path . '/' . 'vespula-cache-' . $key;

        if (! file_exists($filename) || ! is_readable($filename)) {
            return false;
        }

        $file = new \SplFileObject($filename);
        $file->rewind();
        $ttl = (int) $file->current();
        unset($file);

        $modtime = filemtime($filename);
        $expired = $this->isExpired($modtime, $ttl);
        if ($expired) {
            $this->delete($key);
            return false;
        }
        return true;
    }

    /**
     * Update the expiry for a keyed value
     * 
     * @param string $key
     * @param integer $ttl
     * @return bool
     */
    public function touch(string $key, int $ttl): bool
    {
        $this->checkKey($key);

        if (! $this->has($key)) {
            return false;
        }
        
        $filename = $this->path . '/' . 'vespula-cache-' . $key;

        $ttl = str_pad((string) $ttl, 20, '-');

        $file = new \SplFileObject($filename, 'r+');
        $file->rewind();
        $bytes = $file->fwrite($ttl);
        //$file->fflush();
        unset($file);

        return $bytes !== false;
    }
}