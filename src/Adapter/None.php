<?php 
declare(strict_types=1);

namespace Vespula\Cache\Adapter;


/**
 * A simple caching adapter that does nothing. In case you want to turn caching off, but not 
 * mess with your code
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 */
class None extends AbstractAdapter 
{
    public function set($key, $value, $ttl = null)
    {
        return true;
    }

    public function get($key, $default = null)
    {
        return $default;
    }

    public function has($key)
    {
        return false;
    }

    public function delete($key)
    {
        return true;
    }

    public function setMultiple($values, $ttl = null)
    {
        return true;
    }

    public function getMultiple($keys, $default = null)
    {
        $store = [];
        foreach ($keys as $key) {
            $store[$key] = $this->get($key);
        }

        return $store;
    }

    public function deleteMultiple($keys)
    {
        return true;
    }

    public function clear()
    {
        return true;
    }
}