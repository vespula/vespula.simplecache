<?php 
declare(strict_types=1);

namespace Vespula\Cache\Adapter;

use Psr\SimpleCache\CacheInterface;
use Vespula\Cache\Exception\InvalidKeyException;
use DateInterval;
use InvalidArgumentException;

/**
 * Abstract base class that implements PSR-16 cache interface.
 * 
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
abstract class AbstractAdapter implements CacheInterface
{
    /**
     * The default expiration time (time-to-live) in seconds. 0 for no expiry.
     * 
     * @var integer
     */
    protected $default_ttl;

    /**
     * Ensure the key is valid based on PSR rules
     * 
     * @param string $key
     * @throws InvalidKeyException
     */
    protected function checkKey($key): void
    {
        $pattern = '/^[a-zA-Z0-9_\.]{3,64}$/';
        if (! preg_match($pattern, $key)) {
            throw new InvalidKeyException('Invalid Key');
        }
    }

    /**
     * Check each key in an array of keys
     * 
     * @param array $keys
     * @throws InvalidKeyException
     * @return void
     */
    protected function checkKeys(array $keys): void
    {
        foreach ($keys as $key) {
            if (is_int($key)) {
                throw new InvalidKeyException('Invalid Key - found integer');
            }
            $this->checkKey($key);
        }
    }

    /**
     * Determine if the cached object has expired based on the access time,  
     * the age and when the item was added to the cache.
     * 
     * @param integer $modtime When the object was created
     * @param integer $ttl Time to live. How long is the object allowed to be cached (in seconds)
     * @return bool True if expired
     */
    protected function isExpired(int $modtime, int $ttl): bool
    {
        // A ttl of 0 means it does NOT expire, but CAN be deleted if explicitly asked to
        if ($ttl === 0) {
            return false;
        }

        return (time() - $modtime) >= $ttl;
    }

    protected function formatTTL($ttl): int
    {
        $this->validateTTL($ttl);

        if (is_a($ttl, DateInterval::class)) {
            $ttl = $ttl->format('%s');
            return (int) $ttl;
        }

        return $ttl;
    }

    protected function validateTTL($ttl): void
    {
        if (! is_int($ttl) && ! (is_a($ttl, DateInterval::class))) {
            throw new InvalidArgumentException('TTL must be an integer or DateInterval object');
        }
    }
}