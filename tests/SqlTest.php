<?php 
declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use Vespula\Cache\Adapter\Sql as SqlCache;
use Vespula\Cache\Exception\InvalidKeyException;



final class SqlTest extends TestCase
{
    protected $adapter;
    protected $pdo;

    protected function setUp(): void
    {
        
        $this->pdo = new \PDO('sqlite::memory:');
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->createDatabase();

        $this->adapter = new SqlCache($this->pdo, 1200, 'vespula_cache');

    }

    protected function createDatabase()
    {
        $sql = 'CREATE TABLE vespula_cache ('
        . 'id VARCHAR(64), '
        . 'content BLOB, '
        . 'timestamp TEXT, '
        . 'ttl INTEGER'
        . ')';

        $this->pdo->query($sql);
    }
    
    public function testGetDefault()
    {
        
        $expected = 'default value';
        $actual = $this->adapter->get('foo', 'default value');

        $this->assertEquals($expected, $actual);
    }

    public function testSetString()
    {
        
        $expected = 'set value';
        $success = $this->adapter->set('bar', 'set value');

        $this->assertTrue($success);

        $query = "SELECT * from vespula_cache where id = 'bar'";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $row = $stmt->fetchObject();

        $expected = 'set value';

        $actual = unserialize($row->content);

        $this->assertEquals($expected, $actual);

        // Same but with TTL
        $successTtl = $this->adapter->set('baz', 'set value', 1200);
        $this->assertTrue($successTtl);

        $query = "SELECT * from vespula_cache where id = 'baz'";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $row = $stmt->fetchObject();

        $expectedTtl = 1200;
        $actualTtl = $row->ttl;

        $this->assertEquals($expectedTtl, $actualTtl);
        
    }

    public function testSetArray()
    {
        $array = [
            'cat'=>'meow',
            'dog'=>'ruff'
        ];
        $success = $this->adapter->set('pets', $array);

        $this->assertTrue($success);

        $query = "SELECT * from vespula_cache where id = 'pets'";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $row = $stmt->fetchObject();

        $actual = unserialize($row->content);

        $this->assertEquals($array, $actual);
    }

    public function testGet()
    {
        $value = 'wildfire';
        $this->adapter->set('fire', $value);


        $actual = $this->adapter->get('fire');
        $this->assertEquals($value, $actual);

        $int = 10;
        $this->adapter->set('int', $int);


        $actual = $this->adapter->get('int');
        $this->assertEquals($int, $actual);

        // ensure types work.
        $string_int = '10';
        $this->adapter->set('intstring', 10);

        $actual = $this->adapter->get('intstring');
  
        $this->assertNotSame($string_int, $actual);

        // test objects
        $object = new \stdClass();
        $object->foo = 'foo';
        $object->bar = false;

        $this->adapter->set('object', $object);

        $actual = $this->adapter->get('object');
  
        $this->assertEquals($object, $actual);

    }

    public function testHas()
    {
        $this->adapter->set('lorem', 'ipsum');

        $this->assertTrue($this->adapter->has('lorem'));
        $this->assertFalse($this->adapter->has('nonesense'));
    }

    public function testHasExpired()
    {
        $this->adapter->set('lorem', 'ipsum', 1);
        sleep(2);
        $this->assertFalse($this->adapter->has('lorem'));

    }

    public function testInvalidKey()
    {
        $this->expectException(InvalidKeyException::class);

        $this->adapter->set('abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJQLMNOPQRSTUVWXYZ1234567890', 'bad');
        $this->adapter->set('ab', 'bad');
        $this->adapter->set('abcd-', 'bad');
        $this->adapter->set('abcd?', 'bad');
        $this->adapter->set('abcd$', 'bad');
        $this->adapter->set('abcd#', 'bad');
        $this->adapter->set('abcdé', 'bad');
    }
    
    public function testValidKey()
    {
        
        $one = $this->adapter->set('abcdefghijklmnopqrstuvwxyzABCDEFGHIJQLMNOPQRSTUVWXYZ1234567890._', 'good');
        $this->assertTrue($one);
        $two = $this->adapter->set('abc', 'good');
        $this->assertTrue($two);
        $three = $this->adapter->set('123', 'good');
        $this->assertTrue($three);

    }

    public function testExpire()
    {
        $this->adapter->set('hello', 'expired', 1);
        sleep(3);
        $actual = $this->adapter->get('hello', 'default');

        $this->assertEquals('default', $actual);

    }

    public function testDelete()
    {
        $this->adapter->set('deleteme', 'good bye');
        $success = $this->adapter->delete('deleteme');

        $this->assertTrue($success);
        $this->assertFalse($this->adapter->has('deleteme'));

    }
    
    public function testClear()
    {
        $this->adapter->set('cat', 'meow');
        $this->adapter->set('dog', 'ruff');
        $this->adapter->set('sheep', 'bah');

        $this->assertTrue($this->adapter->has('cat'));
        $this->assertTrue($this->adapter->has('dog'));
        $this->assertTrue($this->adapter->has('sheep'));

        $this->adapter->clear();

        $this->assertFalse($this->adapter->has('cat'));
        $this->assertFalse($this->adapter->has('dog'));
        $this->assertFalse($this->adapter->has('sheep'));
    }

    public function testSetMultiple()
    {
        $keys = [
            'cat'=>'meow',
            'dog'=>'ruff',
            'sheep'=>'bah',
        ];

        $success = $this->adapter->setMultiple($keys);

        $this->assertTrue($success);

        $this->assertTrue($this->adapter->has('cat'));
        $this->assertTrue($this->adapter->has('dog'));
        $this->assertTrue($this->adapter->has('sheep'));

    }

    public function testGetMultiple()
    {
        $object = new \stdClass();
        $object->name = 'McLovin';

        $cacheData = [
            'cat'=>'meow',
            'dog'=>'ruff',
            'sheep'=>'bah',
            'horse'=>$object
        ];

        $this->adapter->setMultiple($cacheData);

        

        $keys = [
            'cat',
            'dog',
            'sheep',
            'horse',
            'cow'
        ];
        
        $data = $this->adapter->getMultiple($keys, 'default');

        $this->assertEquals($data['cat'], 'meow');
        $this->assertEquals($data['dog'], 'ruff');
        $this->assertEquals($data['sheep'], 'bah');
        $this->assertEquals($data['horse'], $object);
        $this->assertEquals($data['cow'], 'default');

    }

    public function testDeleteMultiple()
    {
        $cacheData = [
            'cat'=>'meow',
            'dog'=>'ruff',
            'sheep'=>'bah',
        ];

        $this->adapter->setMultiple($cacheData);

        $keys = [
            'cat',
            'dog',
        ];

        $success = $this->adapter->deleteMultiple($keys);

        $this->assertTrue($success);

        $this->assertTrue($this->adapter->has('sheep'));
        $this->assertFalse($this->adapter->has('cat'));
        $this->assertFalse($this->adapter->has('dog'));
    }

    public function testDeleteMultipleKeyNotSet()
    {
        $cacheData = [
            'cat'=>'meow',
            'dog'=>'ruff',
            'sheep'=>'bah',
        ];

        $this->adapter->setMultiple($cacheData);

        $keys = [
            'cat',
            'dog',
            'wildebeest'
        ];

        $success = $this->adapter->deleteMultiple($keys);

        $this->assertTrue($success);

        $this->assertTrue($this->adapter->has('sheep'));
        $this->assertFalse($this->adapter->has('cat'));
        $this->assertFalse($this->adapter->has('dog'));
        $this->assertFalse($this->adapter->has('wildebeest'));
    }

    public function testTouch()
    {
        $this->adapter->set('cow', 'moo', 3600);

        $success = $this->adapter->touch('cow', 60);

        $this->assertTrue($success);

        $query = "SELECT ttl from vespula_cache where id = ?";
        $stmt = $this->pdo->prepare($query);

        $success = $stmt->execute(['cow']);

        $row = $stmt->fetchObject();

        $this->assertEquals(60, $row->ttl);
    }

}
