<?php 
declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use Vespula\Cache\Adapter\Memcached as MemcachedCache;
use Vespula\Cache\Exception\InvalidKeyException;
use Vespula\Cache\Exception\Memcached\InvalidTTLException;


final class MemcachedTest extends TestCase
{
    protected $adapter;

    protected function setUp(): void
    {
        
        $memcached = new \Memcached();
        $memcached->addServer('localhost', 11211);
        $memcached->flush();
        $this->adapter = new MemcachedCache($memcached, 120);

    }

    protected function tearDown(): void
    {
        $this->adapter->getPHPmemcached()->quit();
    }
    
    public function testGetDefault()
    {
        
        $expected = 'default value';
        $actual = $this->adapter->get('foo', 'default value');

        $this->assertEquals($expected, $actual);
    }

    public function testSetString()
    {
        
        $expected = 'set value';
        $success = $this->adapter->set('bar', 'set value');

        $this->assertTrue($success);

        $actual = $this->adapter->get('bar');

        $this->assertEquals($expected, $actual);

        
    }

    public function testSetArray()
    {
        $array = [
            'cat'=>'meow',
            'dog'=>'ruff'
        ];
        $success = $this->adapter->set('pets', $array);

        $this->assertTrue($success);

        $actual = $this->adapter->get('pets');

        $this->assertEquals($array, $actual);
    }

    public function testGet()
    {
        $value = 'wildfire';
        $this->adapter->set('fire', $value);


        $actual = $this->adapter->get('fire');
        $this->assertEquals($value, $actual);

        $int = 10;
        $this->adapter->set('int', $int);


        $actual = $this->adapter->get('int');
        $this->assertEquals($int, $actual);

        // ensure types work.
        $string_int = '10';
        $this->adapter->set('intstring', 10);

        $actual = $this->adapter->get('intstring');
  
        $this->assertNotSame($string_int, $actual);

        // test objects
        $object = new \stdClass();
        $object->foo = 'foo';
        $object->bar = false;

        $this->adapter->set('object', $object);

        $actual = $this->adapter->get('object');
  
        $this->assertEquals($object, $actual);

    }

    public function testHas()
    {
        $this->adapter->set('lorem', 'ipsum');

        $this->assertTrue($this->adapter->has('lorem'));
        $this->assertFalse($this->adapter->has('nonesense'));
    }

    public function testInvalidKey()
    {
        $this->expectException(InvalidKeyException::class);

        $this->adapter->set('abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJQLMNOPQRSTUVWXYZ1234567890', 'bad');
        $this->adapter->set('ab', 'bad');
        $this->adapter->set('abcd-', 'bad');
        $this->adapter->set('abcd?', 'bad');
        $this->adapter->set('abcd$', 'bad');
        $this->adapter->set('abcd#', 'bad');
        $this->adapter->set('abcdé', 'bad');
    }
    
    public function testValidKey()
    {
        
        $one = $this->adapter->set('abcdefghijklmnopqrstuvwxyzABCDEFGHIJQLMNOPQRSTUVWXYZ1234567890._', 'good');
        $this->assertTrue($one);
        $two = $this->adapter->set('abc', 'good');
        $this->assertTrue($two);
        $three = $this->adapter->set('123', 'good');
        $this->assertTrue($three);

    }

    public function testExpire()
    {
        $this->adapter->set('hello', 'expired', 1);
        sleep(3);
        $actual = $this->adapter->get('hello', 'default');

        $this->assertEquals('default', $actual);

    }

    public function testDelete()
    {
        $this->adapter->set('deleteme', 'good bye');
        $success = $this->adapter->delete('deleteme');

        $this->assertTrue($success);
        $this->assertFalse($this->adapter->has('deleteme'));

    }
    
    public function testClear()
    {
        $this->adapter->set('cat', 'meow');
        $this->adapter->set('dog', 'ruff');
        $this->adapter->set('sheep', 'bah');

        $this->assertTrue($this->adapter->has('cat'));
        $this->assertTrue($this->adapter->has('dog'));
        $this->assertTrue($this->adapter->has('sheep'));

        $this->adapter->clear();

        $this->assertFalse($this->adapter->has('cat'));
        $this->assertFalse($this->adapter->has('dog'));
        $this->assertFalse($this->adapter->has('sheep'));
    }

    public function testSetMultiple()
    {
        $keys = [
            'cat'=>'meow',
            'dog'=>'ruff',
            'sheep'=>'bah',
        ];

        $success = $this->adapter->setMultiple($keys);

        $this->assertTrue($success);

        $this->assertTrue($this->adapter->has('cat'));
        $this->assertTrue($this->adapter->has('dog'));
        $this->assertTrue($this->adapter->has('sheep'));

    }

    public function testGetMultiple()
    {
        $object = new \stdClass();
        $object->name = 'McLovin';

        $cacheData = [
            'cat'=>'meow',
            'dog'=>'ruff',
            'sheep'=>'bah',
            'horse'=>$object
        ];

        $this->adapter->setMultiple($cacheData);

        

        $keys = [
            'cat',
            'dog',
            'sheep',
            'horse',
            'cow'
        ];
        
        $data = $this->adapter->getMultiple($keys, 'default');

        $this->assertEquals($data['cat'], 'meow');
        $this->assertEquals($data['dog'], 'ruff');
        $this->assertEquals($data['sheep'], 'bah');
        $this->assertEquals($data['horse'], $object);
        $this->assertEquals($data['cow'], 'default');

    }

    public function testDeleteMultiple()
    {
        $cacheData = [
            'cat'=>'meow',
            'dog'=>'ruff',
            'sheep'=>'bah',
        ];

        $this->adapter->setMultiple($cacheData);

        $keys = [
            'cat',
            'dog',
        ];

        $success = $this->adapter->deleteMultiple($keys);

        $this->assertTrue($success);

        $this->assertTrue($this->adapter->has('sheep'));
        $this->assertFalse($this->adapter->has('cat'));
        $this->assertFalse($this->adapter->has('dog'));
    }

    public function testDeleteMultipleKeyNotSet()
    {
        $cacheData = [
            'cat'=>'meow',
            'dog'=>'ruff',
            'sheep'=>'bah',
        ];

        $this->adapter->setMultiple($cacheData);

        $keys = [
            'cat',
            'dog',
            'wildebeest'
        ];

        $success = $this->adapter->deleteMultiple($keys);

        $this->assertTrue($success);

        $this->assertTrue($this->adapter->has('sheep'));
        $this->assertFalse($this->adapter->has('cat'));
        $this->assertFalse($this->adapter->has('dog'));
        $this->assertFalse($this->adapter->has('wildebeest'));
    }

    public function testInvalidTTL()
    {
        $this->expectException(InvalidTTLException::class);

        $this->adapter->set('foo', 'bar', time());
    }
    
    public function testDateInterval()
    {
        $memcached = new \Memcached();
        $memcached->addServer('localhost', 11211);
        $memcached->flush();
        $interval = new \DateInterval('PT1S');
        $this->adapter = new MemcachedCache($memcached, $interval);

        $this->adapter->set('lorem', 'ipsum', 1);
        sleep(2);
        $this->assertFalse($this->adapter->has('lorem'));
    }

    public function testBadDefaultTTlString()
    {
        $this->expectException(InvalidArgumentException::class);
        $memcached = new \Memcached();
        $memcached->addServer('localhost', 11211);
        $memcached->flush();
        
        $this->adapter = new MemcachedCache($memcached, 'string');

    }

    public function testBadDefaultTTlNull()
    {
        $this->expectException(InvalidArgumentException::class);
        $memcached = new \Memcached();
        $memcached->addServer('localhost', 11211);
        $memcached->flush();
        
        $this->adapter = new MemcachedCache($memcached, null);

    }

    public function testBadDefaultTTlArray()
    {
        $this->expectException(InvalidArgumentException::class);
        $memcached = new \Memcached();
        $memcached->addServer('localhost', 11211);
        $memcached->flush();
        
        $this->adapter = new MemcachedCache($memcached, []);

    }
    
}
