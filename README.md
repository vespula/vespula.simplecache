# README #


This is a simple, flexible PSR-16 compliant simple-cache implementation for PHP.

Adapters include File, Sql, Memcached, and None

Documentation is here: https://vespula.bitbucket.io/cache/

Basic interface for PSR-16 is here: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-16-simple-cache.md


